import React from 'react';
import ReactJson from 'react-json-view'

const xlsxParser = require('xlsx-parse-json');

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { output: {}, hasOutput: false, showNullProperties: false, hideEmptyRows: true, file: {} };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.onSubmitCheckbox = this.onSubmitCheckbox.bind(this);
  }

  onChangeHandler(event) {
    const temp = event.target.files[0];
    this.setState({ file: temp });

    this.parseData(temp);
  }

  parseData(file) {
    xlsxParser.onFileSelection(file, { showNullProperties: this.state.showNullProperties, hideEmptyRows: this.state.hideEmptyRows })
      .then(resp => {
        this.setState({ output: resp });
      });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  onSubmitCheckbox(event) {
    event.preventDefault();

    if (this.state.file.name) {
      this.parseData(this.state.file);
    }
  }

  render() {
    return (<div className="App">
      <form>

        <input multiple type="file" name=".xlsx" onChange={this.onChangeHandler} />
        <br />
        <br />
        <label>
          showNullProperties:
          <input
            name="showNullProperties"
            type="checkbox"
            checked={this.state.showNullProperties}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          hideEmptyRows:
          <input
            name="hideEmptyRows"
            type="checkbox"
            checked={this.state.hideEmptyRows}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <button onClick={this.onSubmitCheckbox}>Submit Checkbox Selection</button>
        <br />
        <br />
        <ReactJson src={this.state.output} theme="rjv-default" />
      </form>
    </div>);
  }
}
export default App;
